<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/work', function () {
    return view('work');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::post('send-email', function (Request $request) {

    $data = array(
        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'messageBody' => $request->input('message'),
        'title' => $request->input('title')
    );

    Mail::send('emails.mail', $data, function ($message) {

        $message->from('upsignform@gmail.com', 'Learning Laravel Subject');

        $message->to('markovski.bojan@hotmail.com')->subject('Learning Laravel test email subject');

    });

    return redirect('/contact');

});
