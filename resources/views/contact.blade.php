<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Contact</title>
	<link rel="shortcut icon" href="images/0.png"/>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/contact.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"></head>
<body>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">

					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-brand" href="/">
									<img src="images/logo-signup.png" class="img-resposive"></a>
							</div>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="col-md-8 col-sm-8 col-xs-8 menu">
							<ul class="nav navbar-nav pull-right menu-show">
								<li>
									<a>
										<div class="hamburger-menu">
											<div class="span-1"></div>
											<div class="span-2"></div>
											<div class="span-3"></div>
										</div>
										MENU
									</a>
								</li>
							</ul>

							<!-- DROPDOWN MENU -->
							<div class="menu-dropdown menu-desktop">
								<ul>
									<li class="pd-bottom">
										<span class="menu-hide"> <i class="fas fa-times"></i>
											close
										</span>
									</li>
									<a href="/home">
										<li class="menu-inner">
											<div>
												<h1>HOME</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/about">
										<li class="menu-inner">
											<div>
												<h1>ABOUT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/work">
										<li class="menu-inner">
											<div>
												<h1>OUR WORK</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/services">
										<li class="menu-inner">
											<div>
												<h1>SERVICES</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/contact">
										<li class="menu-inner">
											<div>
												<h1>CONTACT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<div class="contact">
										<span>FOLLOW US:</span> 
										<a href="https://www.facebook.com/signupdma/?fref=ts">
											<i class="fab fa-facebook-f"></i>
										</a>
									</div>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- MOBILE DROPDOWN -->
			<div class="row wrap-mobile-navbar">
				<div class="col-sm-12 col-xs-12 mobile-navbar">
					<div>
							<div class="pd-bottom list-dropdown-mobile">
								<span class="menu-hide">
									<i class="fas fa-times"></i>
									close
								</span>
							</div>
						<ul>
							<div class="display-flex-menu">
								<a href="/">
									<li class="list-dropdown-mobile">
										<div>
											<h1>HOME</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/about">
									<li class="list-dropdown-mobile">
										<div>
											<h1>ABOUT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/work">
									<li class="list-dropdown-mobile">
										<div>
											<h1>OUR WORK</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/services">
									<li class="list-dropdown-mobile">
										<div>
											<h1>SERVICES</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/contact">
									<li class="list-dropdown-mobile">
										<div>
											<h1>CONTACT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
							</div>
						</ul>
						<div class="contact">
							<span>FOLLOW US:</span>
							<a href="https://www.facebook.com/signupdma/?fref=ts">
								<i class="fab fa-facebook-f"></i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<div class="container-fluid wrap-contact">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
				<!-- TITLE ABOUT US -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h1 class="text-center">
							<span>Contact</span>
						</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-center inner-contact">
						<p>Vladimir Polezinovski br.10 / Skopje 1000</p>
						<p>+389 71 223 035 / +389 71 223 036</p>
						<p>office@signup.mk</p>
					</div>
				</div>
				<form method="POST" action="/send-email">
					@csrf

					<div class="row text-center form-group">
						<div class="col-md-3 col-md-offset-3 col-sm-5 col-sm-offset-1 col-xs-6 col-xs-offset-0">
							<input type="text"  name="name" placeholder="YOUR NAME" class="form-control" maxlength="11" required></div>
						<div class="col-md-3 col-sm-5 col-xs-6">
							<input type="email" name="email" placeholder="E-MAIL" class="form-control" required></div>
						<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
							<input type="text" name="title" placeholder="SUBJECT" class="form-control"></div>
						<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
							<textarea name="message" class="form-control" placeholder="MESSAGE" rows="5" required></textarea>
						</div>
						<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
							<button class="btn btn-block" type="submit">SEND MESSAGE</button>
						</div>
						<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 contact-bottom">
							<span>FOLLOW US:</span> 
							<a href="https://www.facebook.com/signupdma/?fref=ts">
								<i class="fab fa-facebook-f"></i>
							</a>
						</div>
					</div>
				<form>
			</div>
		</div>
	</div>

	<script>

			$(".wrap-contact").fadeIn(1000);
		$( document ).ready()

			// DESKTOP DROPDOWN
			$(".menu-show").on('click', function() {
				$(".menu-dropdown").slideDown();
				$(".wrap-mobile-navbar").slideDown();
				$(".list-dropdown-mobile").slideDown();
			});
			$(".menu-hide").on('click', function() {
				$(".menu-dropdown").slideUp();
			});

			// MOBILE DROPDOWN
			$(".menu-hide").on('click', function() {
				$(".wrap-mobile-navbar").slideUp();
			})

			$(document).mouseup(function (e)
				{
				var container = $(".menu-dropdown"); // YOUR CONTAINER SELECTOR

				if (!container.is(e.target) // if the target of the click isn't the container...
					&& container.has(e.target).length === 0) // ... nor a descendant of the container
				{
				container.slideUp();
				}
			});

			$(document).mouseup(function (e)
				{
				var liMobile = $(".list-dropdown-mobile"); // YOUR CONTAINER SELECTOR

				if (!liMobile.is(e.target) // if the target of the click isn't the container...
					&& liMobile.has(e.target).length === 0) // ... nor a descendant of the container
				{
				$(".wrap-mobile-navbar").slideUp();
				}
			});
		
	</script>
</body>
</html>