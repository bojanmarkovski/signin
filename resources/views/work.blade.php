<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1.0" />
	<title>Our Work</title>
	<link rel="shortcut icon" href="images/0.png"/>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/work.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/blueimp-gallery.min.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/slick/slick.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/slick/slick-theme.css') }}"/>
</head>
<body>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">

					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-brand" href="/">
									<img src="images/logo-signup.png" class="img-resposive"></a>
							</div>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="col-md-8 col-sm-8 col-xs-8 menu">
							<ul class="nav navbar-nav pull-right menu-show">
								<li>
									<a>
										<div class="hamburger-menu">
											<div class="span-1"></div>
											<div class="span-2"></div>
											<div class="span-3"></div>
										</div>
										MENU
									</a>
								</li>
							</ul>

							<!-- DROPDOWN MENU -->
							<div class="menu-dropdown menu-desktop">
								<ul>
									<li class="pd-bottom">
										<span class="menu-hide"> <i class="fas fa-times"></i>
											close
										</span>
									</li>
									<a href="/">
										<li class="menu-inner">
											<div>
												<h1>HOME</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/about">
										<li class="menu-inner">
											<div>
												<h1>ABOUT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/work">
										<li class="menu-inner">
											<div>
												<h1>OUR WORK</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/services">
										<li class="menu-inner">
											<div>
												<h1>SERVICES</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/contact">
										<li class="menu-inner">
											<div>
												<h1>CONTACT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<div class="contact">
										<span>FOLLOW US:</span> 
										<a href="https://www.facebook.com/signupdma/?fref=ts">
											<i class="fab fa-facebook-f"></i>
										</a>
									</div>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- MOBILE DROPDOWN -->
			<div class="row wrap-mobile-navbar">
				<div class="col-sm-12 col-xs-12 mobile-navbar">
					<div>
							<div class="pd-bottom list-dropdown-mobile">
								<span class="menu-hide">
									<i class="fas fa-times"></i>
									close
								</span>
							</div>
						<ul>
							<div class="display-flex-menu">

								<a href="/">
									<li class="list-dropdown-mobile">
										<div>
											<h1>HOME</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/about">
									<li class="list-dropdown-mobile">
										<div>
											<h1>ABOUT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/work">
									<li class="list-dropdown-mobile">
										<div>
											<h1>OUR WORK</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/services">
									<li class="list-dropdown-mobile">
										<div>
											<h1>SERVICES</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/contact">
									<li class="list-dropdown-mobile">
										<div>
											<h1>CONTACT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
							</div>
						</ul>
							<div class="contact">
								<span>FOLLOW US:</span>
								<a href="https://www.facebook.com/signupdma/?fref=ts">
									<i class="fab fa-facebook-f"></i>
								</a>
							</div>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<div class="container-fluid wrap-work">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
				<!-- TITLE ABOUT US -->
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h1 class="text-center">
							<span>OUR WORK</span>
						</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1">
						<div class="row text-center work-category">
							<div class="filter-category">
								<span class="active-category">ALL</span>
							</div>
							<div class="filter-category">
								<span>BRANDING</span>
							</div>
							<div class="filter-category">
								<span>EVENTS</span>
							</div>
							<div class="filter-category">
								<span>PRINT</span>
							</div>
							<div class="filter-category">
								<span>VIDEO</span>
							</div>
							<div class="filter-category">
								<span>WEB</span>
							</div>
						</div>
					</div>
				</div>

				<!-- MODAL -->
				<div id="modal-container">
					
				</div>
				
				<div class="row">
					<div class="col-md-4">
						<div class="modal fade bd-example-modal-lg" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="work">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div id="blueimp-gallery-new" class="blueimp-gallery">
										<div class="slides"></div>

										<button type="button" class="btn close-modal" id="close-modal" data-dismiss="modal"  >&times;</button>
										<a class="prev">‹</a>
										<a class="next">›</a>
										<ol class="indicator"></ol>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

		</div>

		<!-- CONTACT -->
			<div class="row mobile-contact">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="text-center">
						<span>Contact</span>
					</h1>
				</div>
			</div>
			<div class="row mobile-contact">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center inner-contact">
					<p>Vladimir Polezinovski br.10 / Skopje 1000</p>
					<p>+389 71 223 035 / +389 71 223 036</p>
					<p>office@signup.mk</p>
				</div>
			</div>
			<form method="POST" action="/send-email">
				@csrf

				<div class="row text-center form-group mobile-contact">
					<div class="col-md-3 col-md-offset-3 col-sm-5 col-sm-offset-1 col-xs-6 col-xs-offset-0">
						<input type="text" name="name" placeholder="YOUR NAME" class="form-control"maxlength="11" required></div>
					<div class="col-md-3 col-sm-5 col-xs-6">
						<input type="email" name="email" placeholder="E-MAIL" class="form-control" required></div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
						<input type="text" name="title" placeholder="SUBJECT" class="form-control"></div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
						<textarea type="message" class="form-control" placeholder="MESSAGE" rows="5" required></textarea>
					</div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
						<button class="btn btn-block" type="submit">SEND MESSAGE</button>
					</div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 contact-bottom">
						<span>FOLLOW US:</span>
						<a href="https://www.facebook.com/signupdma/?fref=ts">
							<i class="fab fa-facebook-f"></i>
						</a>
					</div>
				</div>
			</form>
	</div>

	<script src="{{ asset('js/blueimp-gallery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('css/slick/slick.js') }}"></script>

	<script>
			$( document ).ready()


			// DESKTOP DROPDOWN
			$(".menu-show").on('click', function() {
				$(".menu-dropdown").slideDown();
				$(".wrap-mobile-navbar").slideDown();
				$(".list-dropdown-mobile").slideDown();
			});
			$(".menu-hide").on('click', function() {
				$(".menu-dropdown").slideUp();
			});

			// MOBILE DROPDOWN
			$(".menu-hide").on('click', function() {
				$(".wrap-mobile-navbar").slideUp();
			})

			$(document).mouseup(function (e)
				{
				var container = $(".menu-dropdown"); // YOUR CONTAINER SELECTOR

				if (!container.is(e.target) // if the target of the click isn't the container...
					&& container.has(e.target).length === 0) // ... nor a descendant of the container
				{
				container.slideUp();
				}
			});

			$(document).mouseup(function (e)
				{
				var liMobile = $(".list-dropdown-mobile"); // YOUR CONTAINER SELECTOR

				if (!liMobile.is(e.target) // if the target of the click isn't the container...
					&& liMobile.has(e.target).length === 0) // ... nor a descendant of the container
				{
				$(".wrap-mobile-navbar").slideUp();
				}
			});


			// MODAL
			function workCategory(categoryName, mainImg, modalImages) {
				this.categoryName = categoryName;
				this.mainImg = mainImg;
				this.modalImages = modalImages;
				this.id = 'work-' + Math.floor(Math.random() * 100);

				this.print = function() {
					var imageContainer = "";

					for (var i = 0; i < this.modalImages.length; i++) {
						imageContainer += `<img src="${this.modalImages[i]}">`
					}

					return 	`	
							<div class="col-md-4 col-sm-4 slider">
								<img src="${this.mainImg}" class="fade-in inner-div img-resposive image-work" data-toggle="modal" data-target="#work-${this.id}" data-work-id="${this.id}">
							</div>
							`
				};
			}

			var signUp   = new workCategory("BRANDING", "../images/work.jpeg", ["../images/work.jpeg", "../images/work.jpeg"]);
			var signUp1  = new workCategory("EVENTS", "../images/work.jpeg", ["../images/work.jpeg", "images/small3.png","../images/work.jpeg", "images/small3.png"]);
			var signUp2  = new workCategory("PRINT", "../images/work.jpeg", ["../images/work.jpeg", "images/small2.png"]);
			var signUp3  = new workCategory("VIDEO", "../images/work.jpeg", ["../images/work.jpeg", "images/small4.png"]);
			var signUp4  = new workCategory("BRANDING", "../images/work.jpeg", ["../images/work.jpeg", "images/small1.png"]);
			var signUp5  = new workCategory("WEB", "../images/work.jpeg", ["../images/work.jpeg", "images/small5.png"]);
			var signUp6  = new workCategory("BRANDING", "../images/work.jpeg", ["../images/work.jpeg", "../images/work.jpeg"]);
			var signUp7  = new workCategory("EVENTS", "../images/work.jpeg", ["../images/work.jpeg", "images/small3.png","../images/work.jpeg", "images/small3.png"]);
			var signUp8  = new workCategory("PRINT", "../images/work.jpeg", ["../images/work.jpeg", "images/small2.png"]);
			var signUp9  = new workCategory("VIDEO", "../images/work.jpeg", ["../images/work.jpeg", "images/small4.png"]);
			var signUp10 = new workCategory("BRANDING", "../images/work.jpeg", ["../images/work.jpeg", "images/small1.png"]);
			var signUp11 = new workCategory("WEB", "../images/work.jpeg", ["../images/work.jpeg", "images/small5.png"]);
			var signUp12 = new workCategory("WEB", "../images/work.jpeg", ["../images/work.jpeg", "images/small5.png"]);

			var categoryWork = [signUp, signUp1, signUp2, signUp3, signUp4, signUp5, signUp6, signUp7, signUp8, signUp9, signUp10, signUp12,signUp, signUp1, signUp2, signUp3, signUp4, signUp5, signUp6, signUp7, signUp8, signUp9];

			var categoryName = null;
			if (window.location.hash != "") {
				categoryName = window.location.hash;

				categoryName = categoryName.substr(1, categoryName.length);

				window.location.hash = "";
			}


			var maxItemsPerSlide = categoryWork.length;
			if (maxItemsPerSlide > 6) {
				maxItemsPerSlide = 6;
			}

			var newArray = [];
			for (var i = 0; i < categoryWork.length; i++) {
				if (categoryWork[i].categoryName == categoryName || !categoryName) {
					newArray.push(categoryWork[i]);
				}
			}

			if (newArray.length <= 0) {
				newArray = categoryWork;
			}

			var slideCounter = 0;
			var outerHtmlSlideContainer = '';
			while(true) {

				var innerHtmlSlides = '';

				innerHtmlSlides += `<div class="row">
										<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">`;
				// Print category
				for (var i = slideCounter * 6; i < maxItemsPerSlide * (slideCounter + 1); i++) {

					if (newArray[i]) {
						innerHtmlSlides += newArray[i].print();
					}
				}
				slideCounter++;

				innerHtmlSlides += `</div></div>`;

				outerHtmlSlideContainer += innerHtmlSlides;

				if (i >= newArray.length) {
					break	
				}
			}

			var finalPrintHtml = `<div class="row pd-top-service display-none-service">
					<div class="col-md-12 col-sm-12 focus-line">
						<div class="slider-container" id="category-container"> 
							${outerHtmlSlideContainer}
						</div>
					</div>
				</div>`;

			document.getElementById("modal-container").innerHTML = finalPrintHtml;

			$('.slider-container').slick({
				dots: true,
				infinite: true,
				speed: 700,
				autoplay: true,
				autoplaySpeed: 5000,
			});

			

			// FILTER
			$(".filter-category").on('click', 'span', function() {
				var category = $(this).text();

				var newArray = [];
				for (var i = 0; i < categoryWork.length; i++) {
					if (category == categoryWork[i].categoryName || category == 'ALL') {
						newArray.push(categoryWork[i]);
					}
				}

				var maxItemsPerSlide = newArray.length;
				if (maxItemsPerSlide > 6) {
					maxItemsPerSlide = 6;
				}

				var slideCounter = 0;
				var outerHtmlSliderContainer = '';
				while(true) {
					var innerHtmlSlides = '';

					innerHtmlSlides += `<div class="row">
											<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">`;
					// Print category
					for (var i = slideCounter * 6; i < maxItemsPerSlide * (slideCounter + 1); i++) {

						if(newArray[i]) {
							innerHtmlSlides += newArray[i].print();
						}

					}
					slideCounter++;

					innerHtmlSlides += `</div></div>`;

					outerHtmlSliderContainer += innerHtmlSlides;

					if (i >= newArray.length) {
						break	
					}
				}

				var finalPrintHtml = `<div class="row pd-top-service display-none-service">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="slider-container" id="category-container"> 
								${outerHtmlSliderContainer}
							</div>
						</div>
					</div>`;

				document.getElementById("modal-container").innerHTML = finalPrintHtml;

				$('.slider-container').slick({
					dots: true,
					infinite: true,
					speed: 700,
					autoplay: true,
					autoplaySpeed: 5000,
				});
				$(".fade-in").addClass("load");
			});


			$("#modal-container").on('click', '.image-work', function() {

				var workId = $(this).attr("data-work-id");

				$('#work').removeClass('fade');
				$('#work').css('display', 'block');
				for(var i = 0; i < categoryWork.length; i++) {

					if(categoryWork[i].id == workId) {

						var carouselLinks = [];
						for(var j = 0; j < categoryWork[i].modalImages.length; j++ ) {
							
							carouselLinks.push({
						        href: categoryWork[i].modalImages[j]
						    })
						}

						blueimp.Gallery(carouselLinks, {
					    	container: '#blueimp-gallery-new',
					    	carousel: true,
					    	indicatorContainer: 'ol',
					    	activeIndicatorClass: 'active',
						    // The list object property (or data attribute) with the thumbnail URL,
						    // used as alternative to a thumbnail child element:
						    thumbnailProperty: 'thumbnail',
						    // Defines if the gallery indicators should display a thumbnail:
						    thumbnailIndicators: true,
						    close: 1,
						    playPauseClass: 'play-pause',
					    })
					}
				}
			})

			// ACTIVE CATEGORY
			$(".row.text-center.work-category div").on('click', function() {

				var categoryItems = $(".row.text-center.work-category").find(".filter-category span");
				for(var i = 0; i < categoryItems.length; i++){
					$(categoryItems[i]).removeClass("active-category");
				}

				$(this).find("span").addClass("active-category");
			})

			$("#close-modal").on('click', function() {

				$('#work').addClass('fade');
				$('#work').css('display', 'none');
			});

			$(".fade-in").addClass("load");

	</script></body>
</html>