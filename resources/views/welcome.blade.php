<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home</title>
	<link rel="shortcut icon" href="images/0.png"/>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"></head>
<body>
	<nav class="navbar">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">

					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-brand" href="/">
									<img src="images/logo-signup.png" class="img-resposive"></a>
							</div>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="col-md-8 col-sm-8 col-xs-8 menu">
							<ul class="nav navbar-nav pull-right menu-show">
								<li>
									<a>
										<div class="hamburger-menu">
											<div class="span-1"></div>
											<div class="span-2"></div>
											<div class="span-3"></div>
										</div>
										MENU
									</a>
								</li>
							</ul>

							<!-- DROPDOWN MENU DESKTOP -->
							<div class="menu-dropdown menu-desktop">
								<ul>
									<li class="pd-bottom">
										<span class="menu-hide"> <i class="fas fa-times"></i>
											close
										</span>
									</li>

									<a href="/">
										<li class="menu-inner">
											<div>
												<h1>HOME</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/about">
										<li class="menu-inner">
											<div>
												<h1>ABOUT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/work">
										<li class="menu-inner">
											<div>
												<h1>OUR WORK</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/services">
										<li class="menu-inner">
											<div>
												<h1>SERVICES</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>
									<a href="/contact">
										<li class="menu-inner">
											<div>
												<h1>CONTACT</h1>
												<div class="line-h1"></div>
											</div>
										</li>
									</a>

									<div class="contact">
										<span>FOLLOW US:</span>
										<a href="https://www.facebook.com/signupdma/?fref=ts">
											<i class="fab fa-facebook-f"></i>
										</a>
									</div>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- MOBILE DROPDOWN -->
			<div class="row wrap-mobile-navbar">
				<div class="col-sm-12 col-xs-12 mobile-navbar">
					<div>
							<div class="pd-bottom list-dropdown-mobile">
								<span class="menu-hide">
									<i class="fas fa-times"></i>
									close
								</span>
							</div>
						<ul>

							<div class="display-flex-menu">
								<a href="/">
									<li class="list-dropdown-mobile">
										<div>
											<h1>HOME</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/about">
									<li class="list-dropdown-mobile">
										<div>
											<h1>ABOUT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/work">
									<li class="list-dropdown-mobile">
										<div>
											<h1>OUR WORK</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/services">
									<li class="list-dropdown-mobile">
										<div>
											<h1>SERVICES</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
								<a href="/contact">
									<li class="list-dropdown-mobile">
										<div>
											<h1>CONTACT</h1>
											<div class="line-h1"></div>
										</div>
									</li>
								</a>
							</div>
						</ul>
							<div class="contact">
								<span>FOLLOW US:</span>
								<a href="https://www.facebook.com/signupdma/?fref=ts">
									<i class="fab fa-facebook-f"></i>
								</a>
							</div>
					</div>
				</div>
			</div>

		</div>
	</nav>

	<div class="container-fluid center-position">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-8  col-xs-offset-2">

				<!-- LEFT SIDE -->
				<div class="row">
					<div class="col-md-4 col-md-offset-0 col-sm-10 col-sm-offset-1 left-side-home">
							<div class="quote-box">
								<div class="quote-loop">
						<h1>
							<div class="slide-h1">
							</div>
									<p>SIGNUP FOR </p> 
						</h1>	
						<h1>
							<div class="slide-h2">
							</div>
									<p>A VOICE, NOT AN </p> 
						</h1>	
						<h1>
							<div class="slide-h3">
							</div>
									<p>ECHO <i class="fas fa-arrow-right fa-1x"></i></p>
						</h1>	
								</div>
								<div class="quote-loop quote-hide">
									<h1>Simple is <br> good <br> idea<i class="fas fa-arrow-right fa-1x"></i></h1>
								</div>
								<div class="quote-loop quote-hide">
									<h1>Let's make <br> nosey <br> neighbors <i class="fas fa-arrow-right fa-1x"></i></h1>
								</div>
								<div class="quote-loop quote-hide">
									<h1>We are <br> pretend <br> to be <i class="fas fa-arrow-right fa-1x"></i></h1>
								</div>
								<div class="quote-loop quote-hide">
									<h1>Perfect <br> is the enemy of <br> good.<i class="fas fa-arrow-right fa-1x"></i></h1>
								</div>
							</div>
						<p class="left-side-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, officiis laborum quo veniam iusto vero dolore ea tenetur, laboriosam repudiandae
						</p>
						<div>
							<button class="btn">OUR WORK >>></button>
						</div>
					</div>

					<!-- RIGHT SIDE -->
					<div class="col-md-8 col-sm-8 right-side-home">
						<div class="boxes">
							<div class="box" data-href="/work#BRANDING">
								<div class="inner-box">
									<div class="img-1"></div>
								</div>
								<p id="grow">BRANDING</p>
							</div>
							<div class="box" data-href="/work#EVENTS">
								<div class="inner-box">
									<div class="img-2"></div>
								</div>
								<p>EVENTS</p>
							</div>
							<div class="box" data-href="/work#WEB">
								<div class="inner-box">
									<div class="img-3"></div>
								</div>
								<p>WEB</p>
							</div>
							<div class="box" data-href="/work#PRINT">
								<div class="inner-box">
									<div class="img-4"></div>
								</div>
								<p>PRINT</p>
							</div>
							<div class="box" data-href="/work#VIDEO">
								<div class="inner-box">
									<div class="img-5"></div>
								</div>
								<p>VIDEO</p>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- MOBILE HOME -->
		<div class="row mobile-home">
			<div class="col-sm-12 col-xs-12" data-href="/work#BRANDING">
				<div class="row" id="box1-mobile">
					<div class="col-sm-10 col-xs-10 box-shadow-home">
						<h2 class="pull-left">BRANDING</h2>
						<img src="images/small1.png" class="pull-right bigger-home"></div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" data-href="/work#EVENTS">
				<div class="row" id="box2-mobile">
					<div class="col-sm-10 col-sm-offset-2 col-xs-10 col-xs-offset-2 box-shadow-home">
						<img src="images/small2.png" class="pull-left bigger-home">
						<h2 class="">EVENTS</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" data-href="/work#WEB">
				<div class="row" id="box3-mobile">
					<div class="col-sm-10 col-xs-10 box-shadow-home">
						<h2 class="pull-left">WEB</h2>
						<img src="images/small3.png" class="pull-right smaller-home"></div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" data-href="/work#PRINT">
				<div class="row" id="box4-mobile">
					<div class="col-sm-10 col-sm-offset-2 col-xs-10 col-xs-offset-2 box-shadow-home">
						<img src="images/small4.png" class="pull-left smaller-home">
						<h2 class="pull-right">PRINT</h2>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-xs-12" data-href="/work#VIDEO">
				<div class="row" id="box5-mobile">
					<div class="col-sm-10 col-xs-10 box-shadow-home">
						<h2 class="pull-left">VIDEO</h2>
						<img src="images/small5.png" class="pull-right smaller-home"></div>
				</div>
			</div>
		</div>

		<!-- CONTACT -->
		<div class="row mobile-contact">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<h1 class="text-center">
					<span>Contact</span>
				</h1>
			</div>
		</div>
		<div class="row mobile-contact">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center inner-contact">
				<p>Vladimir Polezinovski br.10 / Skopje 1000</p>
				<p>+389 71 223 035 / +389 71 223 036</p>
				<p>office@signup.mk</p>
			</div>
		</div>
		<form method="POST" action="/send-email">
				@csrf

				<div class="row text-center form-group mobile-contact">
					<div class="col-md-3 col-md-offset-3 col-sm-5 col-sm-offset-1 col-xs-6 col-xs-offset-0">
						<input type="text" name="name" placeholder="YOUR NAME" class="form-control" maxlength="11" required></div>
					<div class="col-md-3 col-sm-5 col-xs-6">
						<input type="email" name="email" placeholder="E-MAIL" class="form-control" required></div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
						<input type="text" name="title" placeholder="SUBJECT" class="form-control"></div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
						<textarea type="message" class="form-control" placeholder="MESSAGE" rows="5" required></textarea>
					</div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
						<button class="btn btn-block" type="submit">SEND MESSAGE</button>
					</div>
					<div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 contact-bottom">
						<span>FOLLOW US:</span>
						<a href="https://www.facebook.com/signupdma/?fref=ts">
							<i class="fab fa-facebook-f"></i>
						</a>
					</div>
				</div>
			</form>

	</div>

	<script>

			$(window).on('load', function() {
				$(".right-side-home").slideDown(1350);
				// $(".slide-h1").slideUp(1500);
				$(".left-side-home h1").css("opacity", "1");

				$(".slide-h1").animate({
		            'width': '0'
		        },1100, function(){
		            $(".slide-h1").hide();
		        });

		        $(".slide-h2").animate({
		            'width': '0'
		        },1400, function(){
		            $(".slide-h1").hide();
		        });

		        $(".slide-h3").animate({
		            'width': '0'
		        },1700, function(){
		            $(".slide-h1").hide();
		        });
			});
			$("#box1-mobile").fadeIn(1600);
			$("#box2-mobile").fadeIn(1600);
			$("#box3-mobile").fadeIn(1600);
			$("#box4-mobile").fadeIn(1600);
			$("#box5-mobile").fadeIn(1600);

			$(".left-side-home p").fadeIn(2000);
			$(".left-side-home button").fadeIn(2000);



			// DESKTOP DROPDOWN
			$(".menu-show").on('click', function() {
				$(".menu-dropdown").slideDown();
				$(".wrap-mobile-navbar").slideDown();
				$(".list-dropdown-mobile").slideDown();
			});
			$(".menu-hide").on('click', function() {
				$(".menu-dropdown").slideUp();
			});

			// MOBILE DROPDOWN
			$(".menu-hide").on('click', function() {
				$(".wrap-mobile-navbar").slideUp();
			})

			$(document).mouseup(function (e)
				{
				var container = $(".menu-dropdown"); // YOUR CONTAINER SELECTOR

				if (!container.is(e.target) // if the target of the click isn't the container...
					&& container.has(e.target).length === 0) // ... nor a descendant of the container
				{
				container.slideUp();
				}
			});

			$(document).mouseup(function (e)
				{
				var liMobile = $(".list-dropdown-mobile"); // YOUR CONTAINER SELECTOR

				if (!liMobile.is(e.target) // if the target of the click isn't the container...
					&& liMobile.has(e.target).length === 0) // ... nor a descendant of the container
				{
				$(".wrap-mobile-navbar").slideUp();
				}
			});

			$(".box").on('click', function() {
				window.location.href = $(this).attr("data-href");
			})

			$(".mobile-home").on("click", ".col-sm-12.col-xs-12", function() {
				window.location.href = $(this).attr("data-href");
			});

			// QUOTES HOME PAGE
			$(document).ready(function($) {
					if (  $('.quote-loop').length ){
					(function loop() {
						$('.quote-loop').each(function() {
							var $self = $(this);
							$self.parent().queue(function (n) {
								$self.fadeIn(800).delay(2000).fadeOut(800, n);
							});
						}).parent().promise().done(loop);
					}());
				}	
			}); 
	</script>
</body>
</html>