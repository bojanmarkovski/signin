<h3>
    Sender e-mail: {{$email}}
</h3>

<h3>
    Sender name: {{$name}}
</h3>

<h2>
    Title: {{$title}}
</h2>

<p>
    {{$messageBody}}
</p>